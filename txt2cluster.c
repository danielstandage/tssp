/*
--------------------------------------------------------------------------------
Copyright (c) 2011-2013, Daniel S. Standage

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--------------------------------------------------------------------------------
*/

#include "genometools.h"
#include <string.h>
#define BUFFER_SIZE 128

// Unfortunately, we must declare this as a static variable...there is no other
// way to access it in the `compare_gene' function.
GtHashmap *map;

// Prints a gene and all associated TSSs as a row in a table
int print_gene(void *key, void *value, void *data, GtError *error)
{
  const char *gene_id = (const char *)key;
  GtArray *tss_list = (GtArray *)value;
  printf("%s", gene_id);

  unsigned long i;
  for(i = 0; i < gt_array_size(tss_list); i++)
  {
    GtStr *tss_id = *(GtStr **)gt_array_get(tss_list, i);
    printf("\t%s", gt_str_get(tss_id));
  }
  puts("");

  return 0;
}

// Comparison function used for sorting gene/TSS lists
int compare_gene(const char *gene1, const char *gene2)
{
  GtArray *list1 = (GtArray *)gt_hashmap_get(map, gene1);
  GtArray *list2 = (GtArray *)gt_hashmap_get(map, gene2);
  unsigned long size1 = gt_array_size(list1);
  unsigned long size2 = gt_array_size(list2);

  if(size1 > size2)
    return -1;
  else if(size1 < size2)
    return 1;

  return 0;
}

int main(int argc, const char **argv)
{
  // Usage statement
  if(argc > 1)
  {
    fputs( "\nError: this program does not accept command-line arguments.\n"
           "Input is read from STDIN and output is printed to STDOUT.\n"
           "You probably want to use the program like this...\n"
           "\n"
           "  tssp -o data.txt tss.gff3 gene.gff3\n"
           "  txt2cluster < data.txt > data-rows.cluster\n"
           "  tranTSSpose < data-rows.cluster > data-columns.cluster\n"
           "\n"
           "...or in a single command like this.\n"
           "\n"
           "  tssp tss.gff3 gene.gff3 | txt2cluster | tranTSSpose > data.columns.cluster\n\n",
           stderr );
    return 1;
  }
  
  gt_lib_init();
  map = gt_hashmap_new(GT_HASH_STRING, (GtFree)gt_str_delete, (GtFree)gt_str_delete);
  char buffer[BUFFER_SIZE];

  // Load all TSS/gene relationships into memory
  while(fgets(buffer, BUFFER_SIZE, stdin) != NULL)
  {
    if(strlen(buffer) > 1)
    {
      char gb[BUFFER_SIZE], tb[BUFFER_SIZE];
      char *gene_buffer = gb;
      char *tss_buffer  = tb;
      gene_buffer = strtok(buffer, "\t\n");
      tss_buffer  = strtok(NULL, "\t\n");

      GtArray *tss_list;
      GtStr *tss = gt_str_new_cstr(tss_buffer);
      if((tss_list = gt_hashmap_get(map, gene_buffer)) == NULL)
      {
        GtStr *gene = gt_str_new_cstr(gene_buffer);
        tss_list = gt_array_new( sizeof(GtStr *) );
        gt_hashmap_add(map, gt_str_get(gene), tss_list);
      }
      gt_array_add(tss_list, tss);
    }
  }

  // Iterate through genes, ordered by number of TSS
  GtError *error = gt_error_new();
  int result = gt_hashmap_foreach_ordered(map, (GtHashmapVisitFunc)print_gene, NULL, (GtCompare)compare_gene, error);
  if(gt_error_is_set(error))
  {
    fprintf(stderr, "error: %s (result code=%d)\n", gt_error_get(error), result);
    return 1;
  }

  //fprintf(stderr, "Success! (result=%d)\n", result);
  //gt_hashmap_delete(map);
  gt_lib_clean();
  return 0;
}
