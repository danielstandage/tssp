/*
--------------------------------------------------------------------------------
Copyright (c) 2011-2013, Daniel S. Standage

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--------------------------------------------------------------------------------
*/

#include <getopt.h>
#include <string.h>
#include "genometools.h"
#include "extended/feature_node.h"
#include "annotationsketch/feature_index.h"
#include "annotationsketch/feature_index_memory.h"

// Print usage statement for the program
void print_usage(FILE *outstream)
{
  fprintf( outstream,
           "usage: tssp [options] TSSs.gff3 genes.gff3\n"
           "  Options:\n"
           "    -a|--attribute: STRING   For each TSS, print the value of the given attribute\n"
           "                             instead of the start coordinate\n"
           "    -h|--help                Print this help message and exit\n"
           "    -o|--outfile: FILE       Print output to file; default is terminal (STDOUT)\n"
           "    -r|--regionsize: INT     How many nucleotides upstream of the TSS to look\n"
           "                             for associated genes; default is 50kb (50000)\n"
           "    -v|--verbose             Print warnings in addition to errors\n" );
}

// Determine whether the given feature is a TSS feature
bool is_tss_feature(GtFeatureNode *fn)
{
  return gt_feature_node_has_type(fn, "TSS") ||
         gt_feature_node_has_type(fn, "transcription start site");
}

int main(int argc, const char ** argv)
{
  // Option defaults
  const char *outfilename = NULL;
  const char *attr_to_print = "";
  FILE *outfile = stdout;
  unsigned long region_size = 50000;
  bool verbose = false;

  // Parse options from command-line arguments
  int opt = 0;
  int optindex = 0;
  const char *optstr = "a:ho:r:v";
  char * const* args = (char * const*)argv;
  const struct option tssp_options[] =
  {
    { "attribute", required_argument, NULL, 'a' },
    { "help", no_argument, NULL, 'h' },
    { "outfile", required_argument, NULL, 'o' },
    { "regionsize", required_argument, NULL, 'r' },
    { "verbose", no_argument, NULL, 'v' },
    { NULL, no_argument, NULL, 0 },
  };

  for( opt = getopt_long(argc, args, optstr, tssp_options, &optindex);
       opt != -1;
       opt = getopt_long(argc, args, optstr, tssp_options, &optindex) )
  {
    switch(opt)
    {
      case 'a':
        attr_to_print = optarg;
        break;

      case 'h':
        print_usage(stdout);
        return 0;
        break;

      case 'o':
        outfilename = optarg;
        outfile = fopen(outfilename, "w");
        if(outfile == NULL)
        {
          fprintf(stderr, "error: could not open outfile '%s'\n", outfilename);
          return 1;
        }
        break;

      case 'r':
        if( sscanf(optarg, "%lu", &region_size) == EOF )
        {
          fprintf(stderr, "Error: could not convert regionsize '%s' to an unsigned long integer", optarg);
          exit(1);
        }
        break;

      case 'v':
        verbose = true;
        break;
    }
  }
  if(argc - optind != 2)
  {
    fprintf(stderr, "error: must provide two input files\n");
    print_usage(stderr);
    return 1;
  }
  const char **tss_infile = argv + optind;
  const char **ann_infile = argv + optind + 1;

  // Initialize GenomeTools library
  gt_lib_init();
  GtError *error = gt_error_new();

  // Load gene annotations into memory
  GtFeatureIndex *genes = gt_feature_index_memory_new();
  GtNodeStream *ann_instream = gt_gff3_in_stream_new_unsorted(1, ann_infile);
  gt_gff3_in_stream_enable_tidy_mode((GtGFF3InStream *)ann_instream);
  GtGenomeNode *gn;
  int had_error;
  int num_genes = 0;
  int num_seqs = 0;
  while(!(had_error = gt_node_stream_next(ann_instream, &gn, error)) && gn)
  {
    GtFeatureNode *fn = gt_feature_node_try_cast(gn);
    GtRegionNode *rn = gt_region_node_try_cast(gn);
    if(fn && gt_feature_node_has_type(fn, "gene"))
    {
      const char *geneid = gt_feature_node_get_attribute(fn, "ID");
      if(geneid == NULL)
      {
        GtStr *seqid = gt_genome_node_get_seqid(gn);
        GtRange generange = gt_genome_node_get_range(gn);
        fprintf(stderr, "error: gene at %s[%lu, %lu] does not have the required ID attribute\n", gt_str_get(seqid), generange.start, generange.end);
        return 1;
      }
      else
      {
        gt_feature_index_add_feature_node(genes, fn, error);
        if(gt_error_is_set(error))
        {
          fprintf(stderr, "error: %s\n", gt_error_get(error));
          return 1;
        }
        num_genes++;
      }
    }
    else if(rn)
    {
      gt_feature_index_add_region_node(genes, rn, error);
      if(gt_error_is_set(error))
      {
        fprintf(stderr, "error: %s\n", gt_error_get(error));
        return 1;
      }
      num_seqs++;
    }
    else
      gt_genome_node_delete(gn);
  }
  if(had_error)
  {
    fprintf(stderr, "error: %s\n", gt_error_get(error));
    return 1;
  }
  gt_node_stream_delete(ann_instream);
  fprintf(stderr, "Loaded %d genes (from %d sequences) into memory\n", num_genes, num_seqs);

  // Iterate through TSSs, assign each to a gene
  GtNodeStream *tss_instream = gt_gff3_in_stream_new_unsorted(1, tss_infile);
  gt_gff3_in_stream_enable_tidy_mode((GtGFF3InStream *)tss_instream);
  while(!(had_error = gt_node_stream_next(tss_instream, &gn, error)) && gn)
  {
    GtFeatureNode *fn = gt_feature_node_try_cast(gn);
    if(fn && is_tss_feature(fn))
    {
      // Determine the range to search for associated genes, keeping in mind + and - strands
      GtArray *downstream_genes = gt_array_new( sizeof(GtGenomeNode *) );
      GtStr *seqid = gt_genome_node_get_seqid(gn);
      GtRange tss_range = gt_genome_node_get_range(gn);
      GtStrand tss_strand = gt_feature_node_get_strand(fn);
      GtRange search_range = tss_range;
      if(tss_strand == GT_STRAND_FORWARD)
        search_range.end = search_range.start + region_size;
      else if(tss_strand == GT_STRAND_REVERSE)
      {
        if(search_range.end > region_size)
          search_range.start = search_range.end - region_size + 1;
        else
          search_range.start = 0;
      }
      else
      {
        fprintf(stderr, "error: strand '%d' is not specific (GT_STRAND_FORWARD=%d, GT_STRAND_REVERSE=%d)\n", tss_strand, GT_STRAND_FORWARD, GT_STRAND_REVERSE);
        return 1;
      }

      // Grab genes, associate the first one with TSS
      gt_feature_index_get_features_for_range(genes, downstream_genes, gt_str_get(seqid), &search_range, error);
      if(gt_error_is_set(error))
      {
        fprintf(stderr, "error: %s (seqid='%s')\n", gt_error_get(error), gt_str_get(seqid));
        return 1;
      }
      if(gt_array_size(downstream_genes) == 0)
      {
        if(verbose)
        {
          char s = '+';
          if(tss_strand == GT_STRAND_REVERSE)
            s = '-';
          fprintf(stderr, "warning: found TSS with no associated genes (%s[%lu, %lu]%c)\n", gt_str_get(seqid), tss_range.start, tss_range.end, s);
        }
      }
      else
      {
        int i;
        bool found_gene = false;
        if(tss_strand == GT_STRAND_REVERSE)
          gt_array_reverse(downstream_genes);
        for(i = 0; i < gt_array_size(downstream_genes); i++)
        {
          GtFeatureNode *gene = *(GtFeatureNode **)gt_array_get(downstream_genes, i);
          GtStrand gene_strand = gt_feature_node_get_strand(gene);
          if(gene_strand == tss_strand)
          {
            if(strcmp(attr_to_print, "") == 0)
            {
              unsigned long coordinate = tss_range.start;
              if(tss_strand == GT_STRAND_REVERSE)
                coordinate = tss_range.end;
              fprintf(outfile, "%s\t%lu\n", gt_feature_node_get_attribute(gene, "ID"), coordinate);
            }
            else
            {
              const char *attr_to_print_value = gt_feature_node_get_attribute(fn, attr_to_print);
              if(attr_to_print == NULL)
              {
                fprintf(stderr, "error: no '%s' attribute found for TSS on line %u\n", attr_to_print, gt_genome_node_get_line_number(gn));
                return 1;
              }
              fprintf(outfile, "%s\t%s\n", gt_feature_node_get_attribute(gene, "ID"), attr_to_print_value);
            }
            found_gene = true;
            break;
          }
        }
        if(!found_gene && verbose)
        {
          char s = '+';
          if(tss_strand == GT_STRAND_REVERSE)
            s = '-';
          fprintf(stderr, "warning: found TSS with no associated genes (%s[%lu, %lu]%c)\n", gt_str_get(seqid), tss_range.start, tss_range.end, s);
        }
      }
      gt_array_delete(downstream_genes);
    }
    gt_genome_node_delete(gn);
  }
  if(had_error)
  {
    fprintf(stderr, "error: %s\n", gt_error_get(error));
    return 1;
  }
  gt_node_stream_delete(tss_instream);

  // Clean up memory
  fclose(outfile);
  gt_feature_index_delete(genes);
  gt_error_delete(error);
  gt_lib_clean();
  return 0;
}

