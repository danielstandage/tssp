#-------------------------------------------------------------------------------
# Copyright (c) 2011-2013, Daniel S. Standage
# 
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#-------------------------------------------------------------------------------

# Relevant directories
GT_INSTALL_DIR=/usr/local
GT_COMPILE_DIR=/usr/local/src/genometools
INSTALL_DIR=/usr/local

# Binaries
BIN=tssp
CBIN=txt2cluster

# Compilation
CC=gcc
CFLAGS=-Wall -O3 -DWITHOUT_CAIRO
INC=-I /usr/local/src/genometools/src/annotationsketch/ -I $(GT_COMPILE_DIR)/src -I $(GT_INSTALL_DIR)/include/genometools

# Targets
all:		$(BIN) $(CBIN)
		

$(BIN):		tssp.c
		$(CC) $(CFLAGS) $(INC) -o $(BIN) tssp.c -lgenometools

$(CBIN):	txt2cluster.c
		$(CC) $(CFLAGS) $(INC) -o $(CBIN) txt2cluster.c -lgenometools

install:	all
		cp $(BIN) $(CBIN) tranTSSpose $(INSTALL_DIR)/bin/.

clean:		
		rm -f $(BIN) $(CBIN)
